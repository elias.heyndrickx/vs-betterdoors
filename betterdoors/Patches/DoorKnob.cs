namespace BetterDoors.Patches
{
    using HarmonyLib;
    using Vintagestory.API.Common;
    using Vintagestory.API.MathTools;
    using Vintagestory.GameContent;


    [HarmonyPatch(typeof(BlockDoor), "GetSuggestedKnobOrientation")]
    class DoorKnob
    {
        static bool Prefix(IBlockAccessor ba, BlockPos pos, BlockFacing facing, ref string __result, ref BlockDoor __instance)
        {
            Block blockLeft = ba.GetBlock(pos.AddCopy(facing.GetCCW()));
            Block blockRight = ba.GetBlock(pos.AddCopy(facing.GetCW()));

            // Check if there is already a left placed door
            if (__instance.IsSameDoor(blockLeft))
            {
                __result = "left";
                return false;
            }

            // Check if there is already a right placed door
            if (__instance.IsSameDoor(blockRight))
            {
                __result = "right";
                return false;
            }

            // Check for solid and air block for potential future double door (right side)
            if (blockLeft.Id == 0 && blockRight.Id != 0)
            {
                __result = "left";
                return false;
            }

            // Check for solid and air block for potential future double door (left side)
            if (blockLeft.Id != 0 && blockRight.Id == 0)
            {
                __result = "right";
                return false;
            }

            // Default doorknob direction
            __result = "right";
            return false;
        }
    }
}
