namespace BetterDoors.ModSystem
{
    using HarmonyLib;
    using Vintagestory.API.Common;
    using Vintagestory.API.Server;
    using Vintagestory.API.Client;

    public class BetterDoorSystem : ModSystem
    {
        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return true;
        }

        public override void StartClientSide(ICoreClientAPI api)
        {
            // Apply patches with harmony
            var harmony = new Harmony(this.Mod.Info.ModID);
            harmony.PatchAll();
            base.Start(api);
        }
    }
}
